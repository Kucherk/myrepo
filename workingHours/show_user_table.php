
<?php  
    $user_id = $_REQUEST["user_id"];
?>
    <html>
    <head>
        <meta charset='UTF-8'/>
        <title>User table</title>
        <link rel='stylesheet' href='css/style.css' type='text/css'/>
    </head>
    <body>
       <p id="user_id"><?php echo $user_id ?></p>
       <div id="tableContainer">
        <table id="user_table">
            <tr>
                <th colspan='4'><button id='arrowl' onclick="get_prev_month()">&larr;</button>
                                <button id='arrowr' onclick="get_next_month()">&rarr;</button><p id="month"></p>
                </th>
            </tr>
            <tr>
                <th class='hidden_id'>id</th>
                <th>День месяца</th>
                <th>Количество часов</th>
                <th>Комментарий</th>
                <th>Pедактировать</th>
            </tr>
        </table>
    </div>
    <form class='adding_form' action='adding_data.php?user_id=<?php echo $user_id; ?>' method='post'>
        <h3>Добавить запись</h3>
        <div class="formBlock" id="date">
            <label for='add_date'>Дата</label>
            <input type='text' class='adding_form_field numbers' name='add_date'/>
            <label for='add_hours'>Часы</label>
            <input type='text' class='adding_form_field numbers' name='add_hours'/>
        </div>
        <div class="formBlock" id="comments">
            <label for='add_comment'>Комментарий</label>
            <textarea type='text_area' class='adding_form_field' name='add_comment' cols='65' rows='8' placeholder="Ваши комментарии:"></textarea>
        </div>
        <div class='clear'>
            <input class='button_submit' type='submit' value='добавить запись'/>
        </div>
    </form> 
    <script src="Scripts/jquery-3.2.1.min.js"></script>
    <script src="Scripts/script.js"></script>
    </body>
    </html>
    
