<?php
require "database_connection.php";
$data_id = $_GET["data_id"];
$new_hours = $_GET["hours"];
$new_comment = $_GET["comment"];
$add_data_query = "UPDATE Activity
                  SET number_of_hours={$new_hours}, 
                  commentary='{$new_comment}'
                  WHERE id={$data_id};";
$add_data = mysql_query($add_data_query)
or die ("<p> Error in updating data: " . mysql_error() . "</p>");
$get_user_data_query = "SELECT number_of_hours, commentary FROM Activity
                            WHERE id ={$data_id};";
$get_user_data = mysql_query($get_user_data_query)
or die ("<p> Error in pulling new data: " . mysql_error() . "</p>");    
$user_data = mysql_fetch_array($get_user_data);
$new_data = array();
$new_data['hours'] = $user_data[0];
$new_data['comment'] = $user_data[1];

echo json_encode($new_data);