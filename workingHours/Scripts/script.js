var clear         = document.getElementById('clear');
var user_id       = document.getElementById("user_id").innerHTML;
var current_month = document.getElementById("month");
var table         = document.getElementById("user_table");
var date          = new Date;
var date_input    = document.getElementsByName("add_date");
date_input[0].setAttribute('value', date.getDate());
var month         = date.getMonth();
month++;

/*---Проверка имени пользователя и Пароля при регистрации---*/

function check_form_reg(){
    event.preventDefault();
    var name = document.getElementById('user_id_reg').value;
    var password = document.getElementById('user_password_reg').value;
    name = name.trim();
    password = password.trim();
    var substr = " ";
    var spaceN = name.indexOf(substr);
    var spaceP = password.indexOf(substr);
    if(spaceN > 0 || spaceP > 0){
        clear.innerHTML = "User name  and password should be one word!";
        clear_message();
        return;
    }
    if(password.length < 4){
        clear.innerHTML = "User password should be longer then 3 symbols!";
        clear_message();
        return;
    }
    $.getJSON('registration_page.php?create_user_name=' + name + '&create_user_password=' + password, function(data){
        if(!data.id){
            clear.innerHTML = "This user is already exist!";
            clear_message();
            return;
        }else{
            location.replace('show_user_table.php?user_id=' + data.id);
        } 
    });
}

/*---Проверка имени пользователя и Пароля при входе---*/
function check_form(name, password){
    event.preventDefault();
    var name = document.getElementById('user_id').value;
    var password = document.getElementById('user_password').value;
    name = name.trim();
    password = password.trim();
    $.getJSON('user_verification.php?enter_user_name=' + name + '&enter_user_password=' + password, function(data){
        if(!data.id){
            clear.innerHTML = "Wrong number or password!";
            clear_message();
            return;
        }else{
            location.replace('show_user_table.php?user_id=' + data.id);
        }
    });
}

/*---Введение данных на странице пользователя---*/
function data_request(){
    $.getJSON('send_table_data.php?user_id=' + user_id + '&new_month=' + month, function(data){
        current_month.innerHTML = data.month;
        var old_row = document.getElementsByTagName('tr');
        while(old_row.length > 2){
            var i = old_row.length-1;
            old_row[i].parentNode.removeChild(old_row[i]);
        }
        for( var i=0; i < data.counter; i++){
            var row = document.createElement('tr');
            row.innerHTML = "<td class='hidden_id'>" + data.id[i] + "</td><td>"
                                   + data.dates[i] + "</td><td>" 
                                   + data.hours[i] + "</td><td>" 
                                   + data.comments[i] + "</td><td>"
                                   + "<button class='fix'>...</button></td>";
            table.appendChild(row);
            if(i == data.counter-1){
                redact_table();
            }
        }
    });
}


/*---Переключение месяцев---*/
function get_prev_month(){
    --month;
    if(month < 1){
        month = 12;
    }
    data_request();
}
            
function get_next_month(){
    ++month;
    if(month > 12){
        month = 1;
    }
    data_request();
}


/*---Очистка экрана от сообщения об ошибке---*/
function clear_message(){
    window.setTimeout(function(){
        document.getElementById('clear').innerHTML ='';
    }, 4000);  
}


/*---Редактирование записей в таблице---*/
function redact_table(){
    $("#user_table tr td button").bind('click', function(){
        var table_row = $(this).parent().parent();
        var raw_data = table_row.children().slice(0,4);
        var data_id = raw_data[0].innerHTML;
        var hours = raw_data[2].innerHTML;
        var comment = raw_data[3].innerHTML;
        table_row.children().eq(2).empty().append('<input type="text" class="formFix" value="'+ hours +'"/>')
        table_row.children().eq(3).empty().append('<input type="text" class="formFix" value="'+ comment +'"/>');
        table_row.children().eq(4).empty().append('<button id="change">Заменить</button><button id="return">Отмена</button>');
        $('.formFix').eq(0).focus();
        $("#user_table tr td button.fix").each(function(){ 
            $(this).attr('disabled', 'disabled');
        });
        $("button#return").bind('click', function(){
            change_data(table_row, hours, comment);
        });
        $("button#change").bind('click', function(){
            var newData = {};
            newData.data_id = data_id;
            newData.hours   = $('.formFix').eq(0).val();
            newData.comment = $('.formFix').eq(1).val();
            $.getJSON('change_data.php', newData, function(data){
                change_data(table_row, data.hours, data.comment);
            });
        });
    });
}

/*---Внесение изменений в таблицу---*/
function change_data(row, new_hours, new_comment){
    $("#user_table tr td button.fix").each(function(){ 
        $(this).removeAttr('disabled');
    });
    row.children().eq(2).empty().append(new_hours);
    row.children().eq(3).empty().append(new_comment);
    row.children().eq(4).empty().append("<button class='fix'>...</button>");
    redact_table(); 
};

data_request();



